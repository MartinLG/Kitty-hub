class WelcomeController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def index
  	@gitevents_by_types = Array.new
    gitevents_types = Gitevent.distinct(:type)
  	gitevents_types.each do |type|
  		gitevent = {}
  		gitevent["type"] = type
  		gitevent["count"] = Gitevent.where(type: type).count
  		@gitevents_by_types.push(gitevent)
  	end

    map = %Q{
      function() {
        var date = new Date(this.created_at.getFullYear(),
                                         this.created_at.getMonth(),
                                         this.created_at.getDate(),
                                         this.created_at.getHours(),
                                         this.created_at.getMinutes());
        emit(date, {type: this.type, count: 1});
      }
    }

    reduce = %Q{
      function(key, values) {
        var total = {};
        for(var i = 0; i < values.length; i++) {
            if (!(values[i].type in total)) {
                total[values[i].type] = 0;
            }
            total[values[i].type] += values[i].count; 
        }
        return total;
      }
    }

    @gitevents_types_by_date = Gitevent.map_reduce(map, reduce).out(inline: true)

  end

  def update_database
    UpdateDatabase.perform_async(params[:year], params[:month], params[:day], params[:hour])
  end
end 