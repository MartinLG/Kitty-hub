class GiteventsController < ApplicationController
  before_action :set_gitevent, only: [:show, :edit, :update, :destroy]

  # GET /gitevents
  # GET /gitevents.json
  def index
    @gitevents = Gitevent.page params[:page]
  end

  # GET /gitevents/1
  # GET /gitevents/1.json
  def show
  end

  # GET /gitevents/new
  def new
    @gitevent = Gitevent.new
  end

  # GET /gitevents/1/edit
  def edit
  end

  # POST /gitevents
  # POST /gitevents.json
  def create
    @gitevent = Gitevent.new(gitevent_params)

    respond_to do |format|
      if @gitevent.save
        format.html { redirect_to @gitevent, notice: 'Gitevent was successfully created.' }
        format.json { render :show, status: :created, location: @gitevent }
      else
        format.html { render :new }
        format.json { render json: @gitevent.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /gitevents/1
  # PATCH/PUT /gitevents/1.json
  def update
    respond_to do |format|
      if @gitevent.update(gitevent_params)
        format.html { redirect_to @gitevent, notice: 'Gitevent was successfully updated.' }
        format.json { render :show, status: :ok, location: @gitevent }
      else
        format.html { render :edit }
        format.json { render json: @gitevent.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gitevents/1
  # DELETE /gitevents/1.json
  def destroy
    @gitevent.destroy
    respond_to do |format|
      format.html { redirect_to gitevents_url, notice: 'Gitevent was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gitevent
      @gitevent = Gitevent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gitevent_params
      params.require(:gitevent).permit(:id, :type, :public, :created_at)
    end
end
