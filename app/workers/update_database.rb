# app/workers/hard_worker.rb

class UpdateDatabase
  	include Sidekiq::Worker

  	require 'open-uri'
	require 'zlib'
	require 'yajl'

  	def perform(year, month, day, hour)
		Gitevent.collection.drop
		
    	gz = open("http://data.githubarchive.org/"+ year.to_s.rjust(4, "0") +"-"+ month.to_s.rjust(2, "0") +"-"+ day.to_s.rjust(2, "0") +"-"+ hour.to_s.rjust(2, "0") +".json.gz")
		js = Zlib::GzipReader.new(gz).read

		Yajl::Parser.parse(js) do |event|
	  		gitevent = Gitevent.new(event)
	  		gitevent['created_at'] = gitevent['created_at'].to_datetime
	  		gitevent.save
		end
  	end
end