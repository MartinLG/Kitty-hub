json.array!(@gitevents) do |gitevent|
  json.extract! gitevent, :id, :id, :type, :public, :created_at
  json.url gitevent_url(gitevent, format: :json)
end
