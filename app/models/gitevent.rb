class Gitevent
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  field :id, type: String
  field :type, type: String
  field :public, type: Mongoid::Boolean
  field :created_at, type: DateTime
end
