require 'test_helper'

class GiteventsControllerTest < ActionController::TestCase
  setup do
    @gitevent = gitevents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:gitevents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create gitevent" do
    assert_difference('Gitevent.count') do
      post :create, gitevent: { created_at: @gitevent.created_at, id: @gitevent.id, public: @gitevent.public, type: @gitevent.type }
    end

    assert_redirected_to gitevent_path(assigns(:gitevent))
  end

  test "should show gitevent" do
    get :show, id: @gitevent
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @gitevent
    assert_response :success
  end

  test "should update gitevent" do
    patch :update, id: @gitevent, gitevent: { created_at: @gitevent.created_at, id: @gitevent.id, public: @gitevent.public, type: @gitevent.type }
    assert_redirected_to gitevent_path(assigns(:gitevent))
  end

  test "should destroy gitevent" do
    assert_difference('Gitevent.count', -1) do
      delete :destroy, id: @gitevent
    end

    assert_redirected_to gitevents_path
  end
end
